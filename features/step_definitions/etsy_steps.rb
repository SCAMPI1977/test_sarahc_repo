 Given(/^I am on the etsy homepage$/) do
  visit "http://www.etsy.com/uk/"
end

Given(/^I search the product "(.*?)"$/) do |arg1|
  fill_in('search-query', :with => arg1)
	click_on('Search')
end

And(/^I see results returned$/) do
	expect(page).to have_content('Show results for:')
end

When(/^I select first item and add to cart$/) do
  listings = all(".listing-detail a")
  listings.first.click
  	find_button('Add to Cart').click
end

Then(/^I should see an item has been added to cart$/) do
  find_link('Cart').value
end




