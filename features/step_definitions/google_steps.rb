Given(/^I am on the google search page$/) do
  visit "http://www.google.co.uk"
end

When(/^I enter the search term "(.*?)" and search$/) do |arg1|
  fill_in('gbqfq', :with => arg1)
  click_on('gbqfb')
end

Then(/^I should see results returned$/) do
	expect(page).to have_content('Ruby Programming Language')
end
