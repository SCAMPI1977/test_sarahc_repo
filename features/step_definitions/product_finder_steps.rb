Given(/^I have navigated to the productfinder status page$/) do
  visit "http://fulcrum-#{Site::Env}.dave.net-a-porter.com/finder/status"
end

Then(/^I should see the status of the productfinder application$/) do
  app_status = find("pre").text
  response = JSON.parse(app_status)
  response.each{|k, v | puts v}
end

Given(/^I log into Fulcrum (?:as|as a) "([^"]*)"$/) do |user_type|
  fulcrum.login.load
  unless fulcrum.login.logged_in?
    user = load_user_details(user_type)
    fulcrum.login.login(user['username'], user['password'])
  end
end

When(/^I attempt to navigate to an invalid channel$/) do
  visit "http://fulcrum-#{Site::Env}.dave.net-a-porter.com/finder/JCH/search"
end

Then(/^I should be redirected to the login page$/) do
  fulcrum.login.current_path.should == "/user/login"
end

Then(/^"(.*?)" should be returned$/) do |error_message|
  fulcrum.search.invalid_channel_message.text.should == error_message
end

When(/^I navigate to product finder$/) do
  fulcrum.search.load
  wait_for_ajax
end

When(/^I try to access to product finder without logging in$/) do
  fulcrum.search.load
  wait_for_ajax
end

And(/^I choose to select the "(.*?)" channel$/) do |channel|
  fulcrum.search.channel_selector_dropdown.click
  fulcrum.search.select_channel(channel)
end

When(/^I search for pids number "(.*?)"$/) do |pid|
  fulcrum.search.search_field.set(pid)
  fulcrum.search.search_button.click
  wait_for_ajax
  fulcrum.search.upload_lists.each do |list|
    if list.text != "Awaiting Stock List"
      list.click
    else
      puts "EPIC FAIL!"
    end
  end
end

When(/^I search for pid number "(.*?)"$/) do |pid|
  fulcrum.search.search_field.set(pid)
  fulcrum.search.search_button.click
  wait_for_ajax
end

Then(/^the result returned should match "(.*?)"$/) do |pid|
  fulcrum.search.wait_until_search_results_visible
  fulcrum.search.search_results.first.should have_content(pid)
end

When(/^I choose to filter results$/) do
  fulcrum.search.filter_results.click
  fulcrum.search.wait_until_filters_modal_visible
end

When(/^I select "(.*?)" from the "(.*?)" tab$/) do |selection, tab|
  case tab
  when "Designer"
    fulcrum.search.filters_modal.select_tab(tab)
    fulcrum.search.filters_modal.select_designer(selection).click
  when "Product type"
    fulcrum.search.filters_modal.select_tab(tab)
    fulcrum.search.filters_modal.select_product_type(selection).click
  when "Colour"
    fulcrum.search.filters_modal.select_tab(tab)
    fulcrum.search.filters_modal.select_colour(selection).click
  when "Live status"
    fulcrum.search.filters_modal.select_tab(tab)
    fulcrum.search.filters_modal.select_live_status(selection).click
  when "Season"
    fulcrum.search.filters_modal.select_tab(tab)
    fulcrum.search.filters_modal.select_season(selection).click
  end
  fulcrum.search.filters_modal.close_modal.click
  fulcrum.search.wait_until_filters_modal_invisible
end

Then(/^the results should only contain items that are "(.*?)"$/) do |filtered|
  wait_for_ajax
  fulcrum.search.search_result_details.each do |result|
    result.has_text? filtered
  end
end

When(/^I choose to sort by "(.*?)"$/) do |option|
  fulcrum.search.dropdown_toggle.click
  fulcrum.search.sort_by(option)
  sleep 2
end

Then(/^the results should sorted by "(.*?)"$/) do |option|
  case option
  when "PID High to Low"
    fulcrum.search.pid_description[0].text[/\d+/].to_i.should > @fulcrum.search.pid_description[1].text[/\d+/].to_i
  when "Colour"
    my_colour = Array.new
    fulcrum.search.colours.each {|colour| my_colour.push(colour.text)}
    my_colour.sort!
    fulcrum.search.colours.first.text.should == my_colour.first
    fulcrum.search.colours.last.text.should == my_colour.last
  when "Cost High to Low"
    fulcrum.search.default_price(1).gsub(/[EURGBPUSD,.]/, "").to_i.should > fulcrum.search.default_price(2).gsub(/[EURGBPUSD,.]/, "").to_i
  when "Cost Low to High"
    fulcrum.search.default_price(1).gsub(/[EURGBPUSD,.]/, "").to_i.should <= fulcrum.search.default_price(2).gsub(/[EURGBPUSD,.]/, "").to_i
  when "Product Type"
    my_product = Array.new
    fulcrum.search.product_types.each{|product| my_product.push(product.text)}
    my_product.sort!
    fulcrum.search.product_types.first.text.should == my_product.first
    fulcrum.search.product_types.last.text.should == my_product.last
  end
end

When(/^I select a random product$/) do
  fulcrum.search.wait_for_search_results
  wait_for_ajax
  fulcrum.search.select_a_pid_to_move
end

When(/^I assign it to a worklist$/) do
  fulcrum.search.wait_until_move_to_list_banner_visible
  lists = fulcrum.search.move_to_list.count - 1
  selected_list = fulcrum.search.move_to_list[rand(0..lists)]
  net_a_portal.selected_list = selected_list.text
  selected_list.click
  fulcrum.search.move_button.click
end

Then(/^the product should be updated to show that worklist$/) do
  fulcrum.search.wait_for_move_to_list_success_message
  fulcrum.search.search_field.set(fulcrum.search.moved_pid.text)
  fulcrum.search.search_button.click
  wait_for_ajax
  fulcrum.search.upload_lists.first.text.should == net_a_portal.selected_list.upcase
end

When(/^I select (?:a|the) "(.*?)" page$/) do |navigation|
  case navigation
  when "random"
    number = fulcrum.search.paging.count - 1
    page = fulcrum.search.paging[rand(1..number)]
    net_a_portal.last_page = fulcrum.search.last_page.text
    net_a_portal.page_number = page.text
    page.click
  when "next"
    page = fulcrum.search.selected_page.text.to_i + 1
    net_a_portal.page_number = page.to_s
    click_on(page)
  when "previous"
    page = fulcrum.search.selected_page.text.to_i - 1
    net_a_portal.page_number = page.to_s
    click_on(page)
  end
end

Then(/^I should be redirected to that page$/) do
  fulcrum.search.pagination_summary.text.should have_content("Page #{net_a_portal.page_number} of #{net_a_portal.last_page}")
  fulcrum.search.selected_page.text.should == net_a_portal.page_number
  fulcrum.search.should have_search_results
end
