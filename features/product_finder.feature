Feature: Product finder
  As a User
  I want to use product finder
  So I can find products and assign them to worklists

@complete
Scenario: Verifying the status of ProductFinder
  Given I have navigated to the productfinder status page
  Then I should see the status of the productfinder application

@complete
Scenario: Atempting to access product finder without being authenticated
  Given I try to access to product finder without logging in
  Then I should be redirected to the login page

@complete
Scenario: Navigating to an invalid channel from product finder
  Given I log into Fulcrum as "IT God"
  When I navigate to product finder
  And I attempt to navigate to an invalid channel
  Then "Error 404 - Unable to find page" should be returned
  
@complete
Scenario Outline: Searching for a pid within a channel
  Given I log into Fulcrum as "IT God"
  When I navigate to product finder
  And I choose to select the "<channel>" channel
  When I search for pid number "<number>"
  Then the result returned should match "<number>"
Examples:
| channel     | number    |
| NAP Intl    | 1111702   |
| NAP AM      | 457368    |
| NAP APAC    | 457282    |
| Outnet Intl | 1900126   |
| Outnet AM   | 457834    |
| MRP Intl    | 456207    |
| MRP AM      | 455875    |

@complete @slow
Scenario Outline: Sorting products according to the option selected
  Given I log into Fulcrum as "IT God"
  And I navigate to product finder
  When I choose to sort by "<option>"
  Then the results should sorted by "<option>"
Examples:
| option              | 
| PID High to Low     | 
| Colour              | 
| Cost High to Low    | 
| Cost Low to High    |
| Product Type        |

@complete @slow
Scenario Outline: Filtering results according to what has been selected
  Given I log into Fulcrum as "IT God"
  When I navigate to product finder
  And I choose to filter results
  When I select "<choice>" from the "<tab>" tab
  Then the results should only contain items that are "<filtered>"
Examples:
| tab           | choice        | filtered    |
| Designer      | Acne Studios  | Acne Studios|  
| Product type  | Jeans         | Jeans       |
| Colour        | Purple        | Purple      |
| Live status   | Live          | LIVE        |
| Season        | SS14          | SS14        |

@complete
Scenario Outline: Selecting a page and ensuring that the correct page is returned
  Given I log into Fulcrum as "IT God"
  When I navigate to product finder
  And I select a "random" page
  When I select the "<action>" page
  Then I should be redirected to that page
Examples:
| action  |
| next    |
| previous|

@complete
Scenario: Moving a product to a worklist
  Given I log into Fulcrum as a "Prodman Support"
  When I navigate to product finder
  And I select a "random" page
  When I select a random product
  And I assign it to a worklist
  Then the product should be updated to show that worklist