Feature: Test Product search
As a customer
I want to find a product 
So I can add to my bag

Scenario: Search for a product on etsy and add to bag
  Given I am on the etsy homepage 
  And I search the product "dog"
  And I see results returned
  When I select first item and add to cart
  Then I should see an item has been added to cart