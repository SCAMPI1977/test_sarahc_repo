Before do
  Capybara.reset!
  @net_a_portal ||= OpenStruct.new
  @fulcrum = Fulcrum.new
end

After do
  sleep 1.5
end

After do |scenario|
  if(scenario.failed?)
    page.driver.browser.manage.window.maximize
    page.driver.browser.save_screenshot("features/html-reports/#{scenario.__id__}.png")
    embed(File.expand_path("features/html-reports/#{scenario.__id__}.png"), 'image/png', 'SCREENSHOT')
  else
  end
end
