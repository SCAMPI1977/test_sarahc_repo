ENV['DRIVER'] ||= 'chrome_local'
default_driver = ENV['DRIVER'].to_sym

#################################LOCAL#########################################
Capybara.register_driver :firefox_local do |app|
  puts "Running Firefox on Local host #{Time.now.strftime '%Y-%m-%d %H:%M'}"
  profile = Selenium::WebDriver::Firefox::Profile.new
  profile.enable_firebug
  profile["extensions.firebug.allPagesActivation"] = "off"
  Capybara::Selenium::Driver.new(app, :browser => :firefox, :profile => profile)
end

Capybara.register_driver :chrome_local do |app|
  puts "Running Chrome on Local host #{Time.now.strftime '%Y-%m-%d %H:%M'}"
  Capybara::Selenium::Driver.new(app, :browser => :chrome)
end

Capybara.configure do |config|
  config.run_server = false
  config.default_driver = default_driver
  config.default_wait_time = 20
  config.match = :first
  config.exact_options = true
  config.ignore_hidden_elements = true
  config.visible_text_only = false
end