module MyHelper
  def self.prep_cuke_folder(out_dir)
    FileUtils.rm_r(out_dir) unless Dir["#{out_dir}*"].empty?
    Dir.mkdir(out_dir) unless File.exists?(out_dir)
  end

  def load_user_details(user_type)
    user = JSON.parse(IO.read((File.dirname(__FILE__) + '/../../config/user_roles.json')))
    user[user_type]
  end

  def wait_for_ajax
    Timeout.timeout(Capybara.default_wait_time) do
      loop do
        active = page.evaluate_script('jQuery.active')
        break if active == 0
      end
    end
  end

  def net_a_portal
    if defined?(NET_A_PORTAL)
      @net_a_portal ||= NET_A_PORTAL
    else
      @net_a_portal ||= OpenStruct.new
    end
  end

  def fulcrum
  	@fulcrum ||= fulcrum
  end
end

World MyHelper
