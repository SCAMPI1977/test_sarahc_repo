require 'active_support'
require 'capybara'
require 'capybara/cucumber'
require 'capybara/dsl'
require 'capybara/rspec'
require 'cucumber'
require 'cucumber/rake/task'
require 'json'
require 'rake'
require 'require_all'
require 'rspec'
require 'rubygems'
require 'selenium/webdriver'
require 'site_prism'
require 'syntax'
require 'minitest/autorun'
require 'pry'

require_all File.dirname(__FILE__) + '/helpers'
require_all File.dirname(__FILE__) + '/../page_objects'

out_dir = File.dirname(__FILE__) + '/../html-reports/'
MyHelper::prep_cuke_folder(out_dir)