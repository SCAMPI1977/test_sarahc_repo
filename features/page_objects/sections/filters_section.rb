class FiltersSection < SitePrism::Section
	element 	:close_modal, "button.close"
	element 	:tabs, ".nav-tabs"

	def select_tab(name)
		within(tabs) do
			find("a", :text => name).click
			find(".active").text.should == name
		end
	end

	def select_designer(designer)
		find("#facet-options-0 li", :text => designer)
	end

	def select_product_type(product)
		find("#facet-options-1 li", :text=> product)
	end

	def select_colour(colour)
		find("#facet-options-2 li", :text=> colour)
	end

	def select_live_status(status)
		find("#facet-options-3 li", :text=> status)
	end

	def select_season(season)
		find("#facet-options-4 li", :text=> season)
	end
end
