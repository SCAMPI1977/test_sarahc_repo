class SearchResultSection < SitePrism::Section
	element 	:checkbox, "#move_pids"
	element		:pid_description, "h3 a"
	element 	:title_and_status, "h3"
	elements 	:stock, ".search-result-attribute-list"
	element 	:product, :xpath, "//div[1]/div[1]/ul/li[1]"
	element 	:season, :xpath, "//div[1]/div[1]/ul/li[2]"
	element 	:colour, :xpath, "//div[1]/div[1]/ul/li[3]"
	element 	:stock_ordered, :xpath, "//div[1]/div[2]/ul/li[1]"
	element 	:stock_delivered, :xpath, "//div[1]/div[2]/ul/li[2]"
	element 	:stock_available, :xpath, "//div[1]/div[2]/ul/li[3]"
	elements	:columns, ".details-column ul li"

	def product_type(number)
		Capybara.add_selector(:product_type) do
			xpath {|n| "//div[#{n}]/div[1]/ul/li[1]"}
		end
		find(:product_type, number)
	end
end
