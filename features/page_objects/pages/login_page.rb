class Login < SitePrism::Page

  set_url "http://fulcrum-#{Site::Env}.dave.net-a-porter.com/user/login"
  set_url_matcher(/\/user\/login/)

  element :username, "#username"
  element :password, "#password"
  element :login_button, "input[value='Log In']"
  element :user_status, '#user_status'
  element :login_form, '#login_form'
  element :feedback_message, "p.feedback-warning"

  def login(name, pw)
    username.set name
    password.set pw
    login_button.click
  end

  def logged_in?
    user_status.text =~ /Currently logged in as/
  end

  def logged_in_as? (user)
    text_to_match     = "Currently logged in as : " + user
    user_status.text  =~ /#{text_to_match}/
  end

  def secured_login?
    login_form[:action] =~ /^https:/
  end

  def login_failed?
    feedback_message.text =~ /Your login details were not recognised/
  end

  def logout
    logout_link.click
    wait_for_username
  end
end
