class Search < SitePrism::Page
  set_url "http://fulcrum-#{Site::Env}.dave.net-a-porter.com/finder/"
  set_url_matcher(/\/finder/)

  element     :channel_selector_dropdown, ".btn-default"
  elements    :channel_selector, "ul.dropdown-menu li a"
  element     :search_field, "input#products"
  element     :search_button, ".btn-primary"
  element     :dropdown_toggle, ".panel .dropdown-toggle"
  elements    :sort_by_selector, "#sort-by-dropdown li a"
  element     :filter_results, ".btn-default", :text => "Filter results"
  element     :clear_results, ".btn-default", :text => "Clear results"
  element     :select_all, ".btn-default", :text => "Select all"
  element     :pagination_summary, ".pagination-summary"
  elements    :paging, ".pagination li a"
  element     :last_page, ".last a"
  element     :selected_page, ".pagination .active"
  elements    :filter_labels, ".label-primary"
  sections    :search_results, SearchResultSection, ".search-result"
  elements    :colours, :xpath, "//div[1]/div[1]/ul/li[3]"
  elements    :product_types, :xpath, "//div[1]/div[1]/ul/li[1]"
  element     :live, ".label-success", :text=> "LIVE"
  elements    :status_and_visibility, "span.label"
  elements    :search_result_details, ".search-results-details"
  elements    :pid_description, "h3 a"
  elements    :upload_lists, ".upload-list-label"
  elements    :checkboxes, "#move_pids"
  section     :filters_modal, FiltersSection, "div.modal-content"
  element     :spinner, "#spinner"
  element     :move_to_list_banner, "div.toolbar"
  elements    :move_to_list, "#move_to_list option"
  element     :move_button, "button#move-pids"
  element     :move_to_list_success_message, "#request-successful"
  element     :moved_pid, "#request-successful strong"
  element     :invalid_channel_message, "h4"

  def select_channel(channel)
    channels = {
      "NAP Intl" => 0,
      "NAP AM" => 1,
      "NAP APAC" => 2,
      "Outnet Intl" => 3,
      "Outnet AM" => 4,
      "MRP Intl" => 5,
      "MRP AM" => 6
    }
    channel_selector[channels[channel]].click
  end

  def sort_by(option)
    options = {
      "PID High to Low" => 0,
      "Colour" => 1,
      "Cost High to Low" => 2,
      "Cost Low to High" => 3,
      "Product Type" => 4
    }
    sort_by_selector[options[option]].click
  end

  def product_details(result)
    within(search_result_details[result]) do
    end

    def uploads(num)
      within(search_results[num])
      upload_lists.each {|list| puts list.text}
    end
  end

  def product_status(num)
    status_and_visibility[num * 2 - 2].text
  end

  def product_visibility(num)
    status_and_visibility[num * 2 - 1].text
  end

  def default_price(num)
    price = all(:xpath, "//div/div[1]/div[3]/ul/li[1]")
    price[num - 1].text.gsub(/[Default price:]/, "")
  end

  def select_a_pid_to_move
    search_results.map do |result|
      found = result.all(".channel-1 .list-name").find {|list| list.text == "AWAITING STOCK LIST"}
      if found
        return parse_product_id_on(result)
      else
        Exception.new("All products are assigned to a worklist")
      end
    end
  end

  private
  def parse_product_id_on(result)
    result.find('h3 a').text
    result.find('#move_pids').click
  end
end
