class Fulcrum
  def home
    Home.new
  end

  def login
    Login.new
  end

  def search
    Search.new
  end
end
